
OVERVIEW

This module is intended for someone who uses Drupal as a single-user, personal
blogging platform. The fbstatus module provides a way to retrieve your Facebook
"status" setting and publish it on your Drupal website automatically. The
information is synchronized automatically based on a configurable interval. A
Drupal block is created consisting of the status field, nothing more.

This module depends on the availability of an RSS feed of your updates from
Facebook. Recent changes in Facebook discourage the use of RSS and have made it
difficult to discover the URL of this feed, and procedures that work for some
may not work for all. I'm sorry for this, but it's out of my control.

Before installing fbstatus you should follow the URL retrieval procedures below.
If they work for you, proceed with installation of fbstatus. If not, there's
nothing I can do to help.




INSTALLATION

Untar the package and place it in the usual place (generally sites/all/modules).
Activate it, then be sure to set permissions for administration and viewing of
the status.

This module REQUIRES PHP5. The installation process will abort and complain if
you are running an outdated version.

CONFIGURATION

You'll need the URL of Facebook's RSS feed for your status information. This URL
encodes your identity and some security information, and should not be shared
(unless you don't care about making your status info public). The current
process for discovery is as follows:

To find the URL:

1. Log into facebook.

2. Click on "Inbox."

3. Click on the "Notifications" tab.

4. Find the RSS link under "Subscribe to notifications" and copy it. THIS IS NOT
THE RIGHT LINK but it contains essential information.

5. COPY the link URL arguments -- everything to the right of the "?" in the URL.
This should looks something like
id=563407515&viewer=563407515&key=1234aa32e7&format=rss20. Note that the ID and
the viewer values should match.

6. Paste it after http://www.facebook.com/feeds/status.php? to create your feed
URL. Check the feed URL in a browser that is not logged into Facebook to make
sure that it works.

Be sure you have an RSS2.0 URL. This module will not process an Atom feed. 
Copy and paste that URL into your Drupal fbstatus module configuration. Set the
refresh interval and you're good to go. Optionally, you may choose to configure
a timestamp.

The module creates a block that is available in the blocks configuration page.
You'll need to assign it to a region.

By default, the block has no title (you can change this in admin/build/block).
On the Garland theme, the block fits nicely at the top of the left sidebar,
right under your site logo.

The standard Drupal permissions model applies to the retrieved content, so you
can control the audience for this information.

AUTHOR

Steve Yelvington <steve@yelvington.com>
