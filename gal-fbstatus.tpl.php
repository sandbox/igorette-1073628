<?php

/**
 * @file
 * Theme template for a list of msgs.
 *
 * Available variables in the theme include:
 *
 * 1) An array of $msgs, where each msg object has:
 *   $msg->id
 *   $msg->username
 *   $msg->picture
 *   $msg->text
 *   $msg->timestamp
 *
 * 2) $msgkey string containing initial keyword.
 *
 * 3) $title
 *
 */
?>


<div class="social-widget social-widget-profile" id="social-widget-2" style="background-color: #627aad;">
    <div class="social-doc" style="width: 100%;">
        <div class="social-hd">
            <a onclick="window.open(this.href); return false;" href="<?php print 'http://twitter.com/' . $tweets[0]->username; ?>" class="social-profile-img-anchor">
                <img alt="profile" class="social-profile-img" src="<?php print path_to_theme(); ?>/images/sonnenblume_auf_gruen_hh_normal.png">
            </a>
            <h3>GRÜNE Hamburg</h3>
            <h4><a onclick="window.open(this.href); return false;" href="<?php print 'http://twitter.com/' . $tweets[0]->username; ?>"><?php print $tweets[0]->username . " bei Twitter"; ?></a></h4>
        </div>
        <div class="social-bd">
            <div class="social-timeline" style="height: auto;">
                <div class="social-msgs">
                    <div class="social-reference-tweet"></div>

                    <?php if (is_array($tweets)): ?>
                    <?php $msg_count = count($tweets); ?>
                    <?php foreach ($tweets as $tweet_key => $tweet): ?>


                            <div class="social-msg">
                                <div class="social-msg-wrap">
                                    <div class="social-avatar">
                                        <div class="social-img">
                                            <a onclick="window.open(this.href); return false;" href="<?php print 'http://twitter.com/' . $tweet->username; ?>">
                                                <img alt="GRUENE_Hamburg profile" src="<?php print path_to_theme(); ?>/images/sonnenblume_auf_gruen_hh_normal.png">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="social-msg-text">
                                        <p> <?php /* <a onclick="window.open(this.href); return false;" href="<?php print 'http://twitter.com/' . $tweet->username; ?>" class="social-user"><?php print $tweet->username; ?></a> */ ?>
                                    <?php print twitter_pull_add_links($tweet->text); ?>
                                    <em>
                                        <a onclick="window.open(this.href); return false;" class="social-timestamp" href="<?php print 'http://twitter.com/' . $tweet->username . '/status/' . $tweet->id ?>"><?php print $tweet->time_ago ?></a>
                                        ·            <a onclick="window.open(this.href); return false;" class="social-reply" href="http://twitter.com/?status=@"<?php print $tweet->username ?>&amp;in_reply_to_status_id=<?php print $tweet->id ?>&amp;in_reply_to=<?php print $tweet->username ?>">reply</a>
                                    </em>
                                </p>
                            </div>
                        </div>
                    </div>

                    <?php endforeach; ?>
                    <?php endif; ?>

                                    <!-- tweets show here -->
                                </div>
                            </div>
                        </div>
                        <div class="social-ft">
                            <br />
            <?php /*                            <div><a onclick="window.open(this.href); return false;" href="http://twitter.com">
                                      <img alt="" src="http://widgets.twimg.com/i/widget-logo.png">
                                      </a>
                                      <span>
                                      <a onclick="window.open(this.href); return false;" class="social-join-conv" style="color: rgb(255, 255, 255);" href="<?php print 'http://twitter.com/' . $tweet->username; ?>">Join the conversation</a>
                                      </span>
                                      </div> */ ?>
        </div>
    </div>
</div>

